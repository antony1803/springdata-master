package com.anton.demo.repositories;


import com.anton.demo.univer.Subject;
import org.springframework.data.repository.CrudRepository;

public interface SubjectRepository extends CrudRepository<Subject, Integer> {
    Subject findByName(String name);
}
